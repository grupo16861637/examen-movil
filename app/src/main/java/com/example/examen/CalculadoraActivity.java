package com.example.examen;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class CalculadoraActivity extends AppCompatActivity {

    private EditText num1EditText, num2EditText;
    private TextView resultadoTextView, usuarioTextView;
    private Calculadora calculadora;
    private String usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);

        // Recibir el usuario enviado desde MainActivity
        usuario = getIntent().getStringExtra("Cliente");

        // Inicializar vistas
        num1EditText = findViewById(R.id.num1);
        num2EditText = findViewById(R.id.num2);
        resultadoTextView = findViewById(R.id.resultado);
        usuarioTextView = findViewById(R.id.txtUsuario);

        // Mostrar el usuario recibido en el TextView
        usuarioTextView.setText("Usuario: " + usuario);

        Button btnAdd = findViewById(R.id.btnAdd);
        Button btnSubtract = findViewById(R.id.btnSubtract);
        Button btnMultiply = findViewById(R.id.btnMultiply);
        Button btnDivide = findViewById(R.id.btnDivide);
        Button btnLimpiar = findViewById(R.id.btnLimpiar);
        Button btnRegresar = findViewById(R.id.btnRegresar);

        calculadora = new Calculadora();

        btnAdd.setOnClickListener(v -> calcular('+'));
        btnSubtract.setOnClickListener(v -> calcular('-'));
        btnMultiply.setOnClickListener(v -> calcular('*'));
        btnDivide.setOnClickListener(v -> calcular('/'));

        btnLimpiar.setOnClickListener(v -> limpiar());
        btnRegresar.setOnClickListener(v -> regresar());
    }

    private void calcular(char operacion) {
        try {
            float num1 = Float.parseFloat(num1EditText.getText().toString());
            float num2 = Float.parseFloat(num2EditText.getText().toString());

            calculadora.setNum1(num1);
            calculadora.setNum2(num2);

            float resultado = 0;

            switch (operacion) {
                case '+':
                    resultado = calculadora.sumar();
                    break;
                case '-':
                    resultado = calculadora.restar();
                    break;
                case '*':
                    resultado = calculadora.multiplicar();
                    break;
                case '/':
                    resultado = calculadora.dividir();
                    break;
            }

            resultadoTextView.setText("" + resultado);
        } catch (NumberFormatException e) {
            Toast.makeText(this, "Por favor ingrese números válidos", Toast.LENGTH_SHORT).show();
        } catch (IllegalArgumentException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void limpiar() {
        num1EditText.setText("");
        num2EditText.setText("");
        resultadoTextView.setText("");
    }

    private void regresar() {
        finish();
    }
}
