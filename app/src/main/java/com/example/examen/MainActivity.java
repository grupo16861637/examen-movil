package com.example.examen;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class MainActivity extends AppCompatActivity {

    EditText txtUsuario, contraseñaEditText;
    Button btnIngresar, btnCerrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_main);

        // Inicializar vistas
        txtUsuario = findViewById(R.id.txtUsuario);
        contraseñaEditText = findViewById(R.id.contraseña);
        btnIngresar = findViewById(R.id.btnIngresar);
        btnCerrar = findViewById(R.id.btnRegresar);

        // Configurar acción de clic para el botón Ingresar
        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Obtener usuario y contraseña ingresados
                String usuario = txtUsuario.getText().toString();
                String contraseña = contraseñaEditText.getText().toString();

                // Validar usuario y contraseña
                if (usuario.equals(getString(R.string.user)) && contraseña.equals(getString(R.string.pass))) {
                    // Usuario y contraseña válidos, iniciar la actividad CalculadoraActivity
                    Intent intent = new Intent(MainActivity.this, CalculadoraActivity.class);
                    intent.putExtra("Cliente", getString(R.string.nombre));
                    startActivity(intent);
                } else {
                    // Usuario o contraseña incorrectos, mostrar mensaje de error
                    Toast.makeText(MainActivity.this, "Usuario o contraseña incorrectos", Toast.LENGTH_SHORT).show();
                }
            }
        });

        // Configurar acción de clic para el botón Cerrar
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Cerrar la actividad actual
                finish();
            }
        });

        // Ajustar padding para evitar que la barra de estado cubra las vistas
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }
}
