package com.example.examen;

import java.io.Serializable;

public class Calculadora implements Serializable {

    private float num1;
    private float num2;

    // Constructor vacío
    public Calculadora() {
        this.num1=0;
        this.num2=0;
    }

    // Constructor con parámetros
    public Calculadora(float num1, float num2) {
        this.num1 = num1;
        this.num2 = num2;
    }

    public Calculadora(Calculadora calculadora){
        this.num1 = calculadora.num1;
        this.num2 = calculadora.num2;
    }

    // Métodos getters y setters para num1 y num2
    public float getNum1() {
        return num1;
    }

    public void setNum1(float num1) {
        this.num1 = num1;
    }

    public float getNum2() {
        return num2;
    }

    public void setNum2(float num2) {
        this.num2 = num2;
    }

    // Método para sumar num1 y num2
    public float sumar() {
        return num1 + num2;
    }

    // Método para restar num2 a num1
    public float restar() {
        return num1 - num2;
    }

    // Método para multiplicar num1 y num2
    public float multiplicar() {
        return num1 * num2;
    }

    // Método para dividir num1 entre num2
    public float dividir() {
        if (num2 == 0) {
            throw new IllegalArgumentException("El divisor no puede ser cero");
        }
        return num1 / num2;
    }
}
